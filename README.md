# Web-App


### You must have your own access_key and secret_key configured on your machine in order to run this project on your laptop
- First of all my laptop is running Ubuntu. Consider some changes if you are using other GNU/linux distro.
- Install awscli `sudo apt install awscli`
- Then just run `aws configure`
- Configure yours `access_key` and `secret_key`
- Configure the default region and the output format: I chose `us-east-1` and `json`

### You must have terraform to run the setup for this project ###

##### Download and install terraform

```shell
wget https://releases.hashicorp.com/terraform/0.11.10/terraform_0.11.10_linux_amd64.zip
```
```shell
unzip terraform_0.11.10_linux_amd64.zip
```
```shell
unzip terraform_0.11.10_linux_amd64.zip
```
```shell
sudo mv terraform /usr/local/bin/
```
```shell
rm terraform_0.11.10_linux_amd64.zip
```

> *Note*: Test the installation with `terraform version`: The outup should be `Terraform v0.11.10`

#### Starting the infrastructure #####

- Just run: `terraform init` to init terraform and load everything
- Then run: `terraform plan` to see what terraform will build
- Then just: `terraform apply` and `yes` to apply the plan that terraform shows
