#!/bin/bash

## Get the LB Machine IP
LB_IP=(`aws ec2 describe-instances --filters "Name=tag-key,Values=LoadBalancer" --query "Reservations[*].Instances[*].PublicIpAddress" --output=text`)

### SSH in the load balancer machine and start the docker swarm master there
ssh -i ~/.ssh/automation.pem $LB_IP -l ubuntu docker swarm init

# SSH in the lb machine and Get the manager Token
MANAGER_TOKEN=`ssh -i ~/.ssh/automation.pem $LB_IP -l ubuntu docker swarm join-token manager | grep 2377`

# Get the Ips of the WebServers Instances and put in /tmp/instances file
aws ec2 describe-instances --filters "Name=tag-key,Values=WebServer" --query "Reservations[*].Instances[*].PublicIpAddress" --output=text > /tmp/instances

## Iterate in the instances file ips and start the docker swarm master in each machine
for i in `cat /tmp/instances`; do ssh -i ~/.ssh/automation.pem $i -l ubuntu $MANAGER_TOKEN; done

## Get Repository URI
REGISTRY_URI=`aws ecr describe-repositories --repository-names=fashioncloud | jq -c '.repositories[].repositoryUri' | sed -e 's/"//g'`

GET_REGISTRY_LOGIN=`aws ecr get-login --region us-east-1 --no-include-email`
