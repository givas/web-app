resource "aws_route53_zone" "main" {
  name = "fashioncloud.com"
}

resource "aws_route53_record" "www" {
  zone_id = "${aws_route53_zone.main.zone_id}"
  count   = "${var.instance_count}"
  name    = "www.app-${element(var.dns_zone, count.index)}"
  type    = "A"
  ttl     = "300"
  records = ["${element(aws_instance.instances.*.private_ip, count.index)}"]
}
