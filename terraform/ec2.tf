resource "aws_instance" "instances" {
  ami               = "${var.aws_ami}"
  instance_type     = "t3.micro"
  count             = "${var.instance_count}"
  key_name          = "${aws_key_pair.automation.key_name}"
  user_data         = "${file("userdata.sh")}"
  availability_zone = "${element(var.azs, count.index)}"
  security_groups   = ["${aws_security_group.app_firewall.name}"]
  iam_instance_profile  = "${aws_iam_instance_profile.instance_profile.name}"

  tags {
    Name      = "Application_HelloWorld-${element(var.dns_zone, count.index)}"
    App       = "Nginx"
    WebServer = "True"
    Docker    = "True"
  }
}

resource "aws_eip" "eip" {
  instance = "${element(aws_instance.instances.*.id, count.index)}"
  count    = "${var.instance_count}"
}


resource "aws_iam_role" "ec2_instances_role" {
  name = "ec2_instances"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "instance_profile" {
  name  = "instance_profile"
  role = "${aws_iam_role.ec2_instances_role.name}"
}

resource "aws_iam_policy" "iam_policy" {
  name        = "iam_policy"
  policy      = "${file("policy.json")}"
}

resource "aws_iam_policy_attachment" "iam_policy_attach" {
  name       = "iam_policy_attach"
  roles      = ["${aws_iam_role.ec2_instances_role.name}"]
  policy_arn = "${aws_iam_policy.iam_policy.arn}"
}

