resource "aws_instance" "lb" {
  ami             = "${var.aws_ami}"
  instance_type   = "t3.micro"
  count           = "${var.lb_count}"
  key_name        = "${aws_key_pair.automation.key_name}"
  user_data       = "${file("userdata.sh")}"
  security_groups = ["${aws_security_group.lb_firewall.name}"]

  tags {
    Name         = "Load_Balancer"
    App          = "LB"
    LoadBalancer = "True"
    Docker       = "True"
    Swarm_Master = "True"
  }
}

resource "aws_eip" "eip_lb" {
  instance = "${aws_instance.lb.id}"
}
