#!/bin/bash

# Install Docker
# https://docs.docker.com/install/linux/docker-ce/ubuntu/#upgrade-docker-ce
apt-get remove docker docker-engine docker.io

apt-get update -y

apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

apt-key fingerprint 0EBFCD88

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update -y

apt-get install docker-ce -y

usermod -aG docker ubuntu

# Enable docker to always start with the Operational System
systemctl enable docker
