output "instances_ips" {
  value = ["${aws_instance.instances.*.private_ip}"]
}

output "lb_public_ip" {
  value = "${aws_instance.lb.*.public_ip}"
}

output "lb_public_name" {
  value = "${aws_instance.lb.*.public_dns}"
}

output "repository_url" {
  value = "${aws_ecr_repository.docker.repository_url}"
}

output "elastic_ips_instances" {
  value = "${aws_eip.eip.*.public_ip}"
}

output "elastic_ip_lb" {
  value = "${aws_eip.eip_lb.public_ip}"
}
