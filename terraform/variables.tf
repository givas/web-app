variable "aws_ami" {
  description = "The ami to be used to create the ec2 instances"
  default     = "ami-05fb04e2687120d6b"
}

variable "instance_count" {
  description = "Number of ec2 instances to create"
  default     = 2
}

variable "lb_count" {
  description = "Number of ec2 instances to create"
  default     = 1
}

variable "dns_zone" {
  default = ["a", "b"]
}

variable "azs" {
  default = ["us-east-1a", "us-east-1b"]
}
