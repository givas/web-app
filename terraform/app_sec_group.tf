resource "aws_security_group" "app_firewall" {
  name        = "app_firewall"
  description = "Allow only ssh access from outside"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "rules" {

    type = "ingress" 
    from_port       = 0
    to_port         = 0
    protocol        = -1
    security_group_id = "${aws_security_group.lb_firewall.id}"
    source_security_group_id = "${aws_security_group.app_firewall.id}"
}

resource "aws_security_group" "lb_firewall" {
  name        = "lb_firewall"
  description = "Allow ssh  and http on 80 from outside"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "rule_lb" {

    type = "ingress"
    from_port       = 0
    to_port         = 0
    protocol        = -1
    security_group_id = "${aws_security_group.app_firewall.id}"
    source_security_group_id = "${aws_security_group.lb_firewall.id}"
}

